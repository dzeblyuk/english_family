var isMobile = false;

/* $(window).on('load', function () {
  var $preloader = $('#p_prldr');
  $preloader.delay(1000).fadeOut('slow');
}); */

$(document).ready(function () {

  if ($('body').width() < 540) {
    isMobile = true;
  }
  
  $('.phones').mask("+38 (000) 000-00-00"), { autoclear: false }; //.val('+380 (__) ___-__-__');
  

  $(window).scroll(function () {
    if ($(this).scrollTop() > 300) {
      $('.scrollup').fadeIn();
    } else {
      $('.scrollup').fadeOut();
    }
  });

  $('.scrollup').click(function () {
    $("html, body").animate({
      scrollTop: 0
    }, 1000);
    return false;
  });

  $('.slider').slick({
    dots: true,
    arrows: true,
    //autoplay: true,
    infinite: true,
    speed: 1000,
    slidesToShow: 1,
    slidesToScroll: 1,
    prevArrow: "<button type='button' role='button' class='slick-prev' data-role='none' aria-label='Previous'></button>",
    nextArrow: "<button type='button' role='button' class='slick-next' data-role='none' aria-label='Next'></button>",
    responsive: [{
        breakpoint: 1024,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          infinite: true,
          dots: true
        }
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          arrows: false
        }
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          arrows: false
        }
      }
      // You can unslick at a given breakpoint now by adding:
      // settings: "unslick"
      // instead of a settings object
    ]
  });

  
  $('.slider-for').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    fade: true,
    asNavFor: '.slider-nav'
  });
  $('.slider-nav').slick({
    slidesToShow: 2,
    slidesToScroll: 1,
    asNavFor: '.slider-for',
    dots: false,
    centerMode: false,
    focusOnSelect: true,
    prevArrow: "<button type='button' class='slick-prev'></button>",
    nextArrow: "<button type='button' class='slick-next'></button>",
    responsive: [{
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
    ]
  });

  $('.sliderD').slick({
    dots: false,
    arrows: true,
    infinite: true,
    speed: 1000,
    slidesToShow: 4,
    slidesToScroll: 1,
    prevArrow: "<button type='button' class='slick-prev'><i class='fas fa-chevron-circle-left'></i></button>",
    nextArrow: "<button type='button' class='slick-next'><i class='fas fa-chevron-circle-right'></i></button>",
    responsive: [{
        breakpoint: 980,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 1
        }
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
    ]
  });

  $('.slider-for2').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    speed: 1500,
    fade: true,
    arrows: true,
    dots: true,
    infinite: true,
    asNavFor: '.slider-nav2',
    prevArrow: "<button type='button' class='slick-prev'><i class='fas fa-chevron-circle-left'></i></button>",
    nextArrow: "<button type='button' class='slick-next'><i class='fas fa-chevron-circle-right'></i></button>"
  });

  $('.slider-nav2').slick({
    slidesToShow: 3,
    slidesToScroll: 1,
    speed: 1500,
    infinite: true,
    asNavFor: '.slider-for2',
    centerMode: true,
    responsive: [{
      breakpoint: 480,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
        centerMode: false
      }
    }
    ]
  });

  $('.slider_courses').slick({
    dots: false,
    arrows: true,
    infinite: true,
    speed: 1000,
    slidesToShow: 3,
    slidesToScroll: 1,
    prevArrow: "<button type='button' class='slick-prev'><i class='fas fa-chevron-circle-left'></i></button>",
    nextArrow: "<button type='button' class='slick-next'><i class='fas fa-chevron-circle-right'></i></button>",
    responsive: [{
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
    ]
  });


  $('.open_popup').click(function () {
    $('.section_form').show();
    $('body').css('overflow', 'hidden');
  })

  $('.dot-close, .courses_wrapper').click(function () {
    $('.section_form').hide();
    $('body').css('overflow', 'auto');
  })

/*  бургер */
  $(".toggle__menu").click(function () {
    $(".sandwich").toggleClass("active");
  });

  $(".top_nav-menu ul a").click(function () {
    $(".top_nav-menu").fadeOut(600);
    $(".sandwich").toggleClass("active");
  });

  
  $(".toggle__menu").click(function () {
    if ($(".collapsed_list").is(":visible")) {
      $(".collapsed_list").css('display', 'none');//.fadeOut(600);
      $(".tog_button").css('display', 'none');
      $('body').css('overflow', 'auto');
    } else {
      $(".tog_button").css('display', 'block');
      $(".collapsed_list").css('display', 'flex');//fadeIn("slow");
      $('body').css('overflow', 'hidden');
    };
  });

  var groupIndex = 0;
  var groupIndex2 = $(".switch-check").length ? 1 : 0;

  $(".calc_button").click(function () {
    groupIndex = $(".calc_button2").length ? 2 * $(".calc_button").index(this) : $(".calc_button").index(this) + 1;
    $(".active_calc").removeClass("active_calc");
    $(this).addClass("active_calc");
    $("div[class^='card_text']").hide();
    $(".card_text" + (groupIndex + groupIndex2)).show();
    console.log(groupIndex + groupIndex2);
  });

  if ($(".switch-check").length) {
    $(".switch-check").click(function () {
      groupIndex2 = $(".switch-check").is(':checked') + 1;
      $(".active_calc2").removeClass("active_calc2");
      $(".calc_button2:eq( " + (groupIndex2 - 1) + " )").addClass("active_calc2");
      $("div[class^='card_text']").hide();
      $(".card_text" + (groupIndex + groupIndex2)).show();
      console.log(groupIndex + groupIndex2);
    });
  }

  

  $('.dropdown-el').click(function (e) {
    e.preventDefault();
    e.stopPropagation();
    $(this).toggleClass('expanded');
    $('#' + $(e.target).attr('for')).prop('checked', true);
  });
  $(".courses_wrapper").click(function () {
    $('.dropdown-el').removeClass('expanded');
  });
  
  
  $('.call_to_action2 .opendoor')
  .mouseover(function () {
    $('.doors').css({'transform': 'rotateY(0deg)', 'box-shadow': '-15px 0px 45px 0px #fff' });
  })
  .mouseout(function () {
    $('.doors').css('transform', 'rotateY(90deg)');
  });

});





//Массив вопросов и ответа
var data_array = [
  [
    ["This is my friend._____French", "He's", "They're", "It's", "He are", 1],
    ["Jim _____ have a car.", "doesn't", "isn't", "don't", "not", 1],
    ["I can ____ Spanish.", "speaking", "to speak", "speak", "spoke", 3],
    ["Anna ___ in a bank.", "work", "works", "is work", "working", 2],
    ["I didn't ____ TV last night.", "watched", "watching", "watch", "not watched", 3],
    ["Alex loves giving presents. He's very ____.", "selfish", "shy", "lazy", "generous", 4],
    ["If you want to get fit, you ___ do more sport.", "will", "would", "should", "have", 3],
    ["Many types of watches ____ in Switzerland.", "are made", "made", "are making", "is made", 1],
    ["What would you do if you ___ a million pounds?", "win", "would win", "won", "winning", 3],

  ],

  [
    ["If you want to get fit, you ___ do more sport.", "will", "would", "should", "have", 3],
    ["Many types of watches ____ in Switzerland.", "are made", "made", "are making", "is made", 1],
    ["What would you do if you ___ a million pounds?", "win", "would win", "won", "winning", 3],
    ["If you go to London, the Tate Modern is really worth ____.", "to see", "seeing", "to be seen", "see", 2],
    ["I ____ home yesterday when it started raining.", "walk", "was walking", "have walked", "have been walking", 2],
    ["I wish I ____ get up early every morning!", "didn't have to", "mustn't", "hadn't", "wouldn't have to", 1],
    ["If I ____ earlier, I wouldn't have been late for work.", "left", "was leaving", "had left", "have left", 3],
    ["This time next week I ____ on the beach on holiday!", "will go to sit", "am sitting", "will sit", "will be sitting", 4],
    ["By 2030 I believe we will ___ a settlement on Mars.", "have built", "have been building", "be built", "been built", 1],

  ],

  [
    ["If you go to London, the Tate Modern is really worth ____.", "to see", "seeing", "to be seen", "see", 2],
    ["I ____ home yesterday when it started raining.", "walk", "was walking", "have walked", "have been walking", 2],
    ["I wish I ____ get up early every morning!", "didn't have to", "mustn't", "hadn't", "wouldn't have to", 1],
    ["If I ____ earlier, I wouldn't have been late for work.", "left", "was leaving", "had left", "have left", 3],
    ["This time next week I ____ on the beach on holiday!", "will go to sit", "am sitting", "will sit", "will be sitting", 4],
    ["By 2030 I believe we will ___ a settlement on Mars.", "have built", "have been building", "be built", "been built", 1],
    ["As soon as I ____ this book, I'll help you clean the car.", "have finished", "will have finished", "am going to finish", "will finish", 1],
    ["It's time ___ the fact that he'll never make it a a rock musician.", "he accepts", "he'll accept", "he accepted", "he'd accept", 3],
    ["Kim still hasn't got ____ to starting his essay.", "through", "off", "over", "round", 4],

  ]
];

var plus = 0;
var time = 0;
var cur_answer = 0;
var commonTestIndex = 0;
var count_answer = data_array[0].length;

function sec() {
  time++;
  document.getElementById('time').innerHTML = 'Затрачено времени: ' + time + ' сек';
}

function start(index) {

    commonTestIndex = index;
    document.getElementById('time').style.display = 'block';
    document.getElementById('option1').style.display = 'block';
    document.getElementById('option2').style.display = 'block';
    document.getElementById('option3').style.display = 'block';
    document.getElementById('option4').style.display = 'block';
    document.getElementById('question').style.display = 'block';

    document.getElementById('option1').innerHTML = data_array[index][cur_answer][1];
    document.getElementById('option2').innerHTML = data_array[index][cur_answer][2];
    document.getElementById('option3').innerHTML = data_array[index][cur_answer][3];
    document.getElementById('option4').innerHTML = data_array[index][cur_answer][4];
    document.getElementById('question').innerHTML = data_array[index][cur_answer][0];

    document.getElementById('but_line').style.display = 'none';
    document.getElementById('end').style.display = 'inline';
    document.getElementById('progressbar').style.display = 'block';

    var intervalID = setInterval(sec, 1000);

};

function check(num) {
  var b = document.getElementById("progressbar");
  var w = b.clientWidth;
  if (num == data_array[commonTestIndex][cur_answer][5]) {
    plus++;    
    
  }
  cur_answer++;
  if (cur_answer === 0) {
    w = 12;
  } else {
    w = 12 * cur_answer;
  };
  if (cur_answer < count_answer) {
    b.style.width = w + "%";
    document.getElementById('option1').innerHTML = data_array[commonTestIndex][cur_answer][1];
    document.getElementById('option2').innerHTML = data_array[commonTestIndex][cur_answer][2];
    document.getElementById('option3').innerHTML = data_array[commonTestIndex][cur_answer][3];
    document.getElementById('option4').innerHTML = data_array[commonTestIndex][cur_answer][4];
    document.getElementById('question').innerHTML = data_array[commonTestIndex][cur_answer][0];
 
  } else {

    //document.getElementById('progressbar').style.width = '100%';
    document.getElementById('time').id = 'stop';
    document.getElementById('option1').style.display = 'none';
    document.getElementById('option2').style.display = 'none';
    document.getElementById('option3').style.display = 'none';
    document.getElementById('option4').style.display = 'none';
    document.getElementById('question').style.display = 'none';
    document.getElementById('testend').style.display = 'inline-block';
    document.getElementById('end').style.display = 'block';
    var percent = Math.round(plus / count_answer * 100);

    var res = [
      [
        ["<span class='green'>Freshman</span> <br><br> <span class='purp'>Oops!</span> Уровень владения английским языком оставляет желать лучшего. Почему бы не исправить это прямо сейчас? Добро пожаловать! "],
        ["<span class='green'>Elementary</span> <br><br><span class='cur_answer'>Попробуйте пройти тест на уровень выше.</span> <br> <span class='purp'>Well done!</span> Ваш английский хорош, но есть к чему стремиться. Доведите его до совершенства с нами."],
        ["<span class='green'>Pre-intermediate</span> <br><br> <span class='purp'>Wow!</span> Впечатляющий результат! До свободного владения языком осталось несколько шагов  – сделайте их с нами! "]
      ],
      [
        ["<span class='green'>Pre-intermediate</span> <br><br><span class='cur_answer'>Попробуйте пройти тест на уровень проще.</span> <br> <span class='purp'>Oops!</span> Уровень владения английским языком оставляет желать лучшего. Почему бы не исправить это прямо сейчас? Добро пожаловать! "],
        ["<span class='green'>Intermediate</span> <br><br> <span class='purp'>Well done!</span> Ваш английский хорош, но есть к чему стремиться. Доведите его до совершенства с нами."],
        ["<span class='green'>Upper-intermediate</span> <br><br><span class='cur_answer'>Попробуйте пройти тест на уровень выше.</span> <br> <span class='purp'>Wow!</span> Впечатляющий результат! До свободного владения языком осталось несколько шагов  – сделайте их с нами! "]
      ],
      [
        ["<span class='green'>Intermediate</span> <br><br><span class='cur_answer'>Попробуйте пройти тест на уровень проще.</span> <br> <span class='purp'>Oops!</span> Уровень владения английским языком оставляет желать лучшего. Почему бы не исправить это прямо сейчас? Добро пожаловать! "],
        ["<span class='green'>Upper-intermediate</span> <br><br> <span class='purp'>Well done!</span> Ваш английский хорош, но есть к чему стремиться. Доведите его до совершенства с нами."],
        ["<span class='green'>Advanced</span> <br><br> <span class='purp'>Wow!</span> Впечатляющий результат! До свободного владения языком осталось несколько шагов  – сделайте их с нами!"]
      ]
    ];
    console.log(percent);
    if (percent > 50) finalIndex = 1;
    else if (percent == 100) finalIndex = 2;
    else finalIndex = 0;

    document.getElementById('result').innerHTML = 'Правильных ответов: ' + plus + ' из ' + count_answer + ' (' + percent + '%)<br><br>' + 'Ваш уровень - ' + res[commonTestIndex][finalIndex];
  }
  
};

//--------------------------------------------------
function TimerQueue() {
  self.currentTimer = null;
  this.tasks = [];
}

TimerQueue.prototype.addTask = function (callback, delay) {
  this.tasks.push({ callback: callback, delay: delay });

  // If there's a scheduled task, bail out.
  if (this.currentTimer) return;

  // Otherwise, start kicking tires
  this.launchNextTask();
};

TimerQueue.prototype.launchNextTask = function () {

  // If there's a scheduled task, bail out.
  if (this.currentTimer) return;

  var self = this;
  var nextTask = this.tasks.shift();

  // There's no more tasks, clean up.
  if (!nextTask) return this.clear();

  // Otherwise, schedule the next task.
  this.currentTimer = setTimeout(function () {
    nextTask.callback.call();

    self.currentTimer = null;

    // Call this function again to set up the next task.
    self.launchNextTask();
  }, nextTask.delay);
};

TimerQueue.prototype.clear = function () {
  if (this.currentTimer) clearTimeout(this.currentTimer);

  // Timer clears only destroy the timer. It doesn't null references.
  this.currentTimer = null;

  // Fast way to clear the task queue
  this.tasks.length = 0;
};

function changeCircle(percentage) {
  var val = parseInt(percentage);
  var $circle = $('#svg #bar');

  if (isNaN(val)) {
    val = 100;
  }
  else {
    var r = $circle.attr('r');
    var c = Math.PI * (r * 2);

    if (val < 0) { val = 0; }
    if (val > 100) { val = 100; }

    var pct = ((100 - val) / 100) * c;
    $circle.css({ strokeDashoffset: pct });
    if (val == 100) {
      var queue = new TimerQueue();
      queue.addTask(function () {
        $("#svg").fadeOut(600);
        $(".preloader").fadeOut(600);
        $('body').css('overflow', 'auto');
      }, 700);

    }
  }
}

function getRandomColor() {
  var letters = '0123456789ABCDEF';
  var color = '#';
  for (var i = 0; i < 6; i++) {
    color += letters[Math.floor(Math.random() * 16)];
  }
  return color;
}