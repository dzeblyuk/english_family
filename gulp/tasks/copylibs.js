'use strict';

module.exports = function() {
	$.gulp.task('copylibs', function() {
		
		return $.gulp.src(['./source/libs/**/*']).pipe($.gulp.dest($.config.root + '/assets/libs'));

	});
};